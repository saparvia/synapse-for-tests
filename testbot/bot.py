"""
Simple Matrix bot that behaves in a deterministic way
"""
from __future__ import print_function

import sys
import time
import logging
import os.path

import matrix_client.client
import matrix_client.errors

logging.info('Connecting')

while True:
    try:
        client = matrix_client.client.MatrixClient("https://localhost:8448", valid_cert_check=False)
        # New user
        token = client.register_with_password(username="testbot", password="monkey")
        break
    except:
        logging.info('Failed to connect, retrying...')
        time.sleep(2)

def invited(room_id, state):
    """Function that is called when an invitation is received"""
    logging.info('Received invitation')
    time.sleep(3) # Sleep a bit so clients can test invited status
    room = client.join_room(room_id)
    room.send_text('Hi there!')


def main():
    """Main function"""
    user = client.get_user('@testbot:localhost')
    user.set_display_name('Testbot!')

    avatar_data = open('%s/avatar.png'%os.path.dirname(__file__)).read()
    avatar_mxurl= client.upload(avatar_data, 'image/png')

    user.set_avatar_url(avatar_mxurl)

    room = client.create_room('testroom1', is_public=True)
    room.set_room_name('Testroom #1')
    room.set_room_topic('This is a test room')

    avatar_data = open('%s/room_avatar.png'%os.path.dirname(__file__)).read()
    avatar_mxurl= client.upload(avatar_data, 'image/png')
    room.send_state_event('m.room.avatar', { 'url': avatar_mxurl }, '')

    client.add_invite_listener(invited)

    logging.info('Listening...', file=sys.stderr)
    client.start_listener_thread()

    while True:
        time.sleep(1)

main()
