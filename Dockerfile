FROM silviof/docker-matrix

MAINTAINER Stefan Parviainen <pafcu@iki.fi>

COPY testbot/ /tmp/testbot
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["start"]
EXPOSE 8448

# Download matrix-python-sdk and copy it to the right place
RUN apt-get update && apt-get install -y wget \
    && wget https://github.com/matrix-org/matrix-python-sdk/archive/master.tar.gz \
    && tar xavf master.tar.gz \
    && cp -r matrix-python-sdk-master/matrix_client /tmp/testbot/matrix_client
