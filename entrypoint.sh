#!/bin/bash
# The script that is run when the docker image is run

OPTION="${1}"

if [[ "${OPTION}" == "start" ]]; then
    # Generate config & certificates
    SERVER_NAME='localhost' REPORT_STATS='no' /start.sh "generate"

    # Change some settings
    sed -i 's/enable_registration:/enable_registration: True #/' /data/homeserver.yaml
    sed -i 's/allow_guest_access:/allow_guest_access: True #/' /data/homeserver.yaml

    # Run bot
    python /tmp/testbot/bot.py &

    # Start Synapse
    /start.sh ${OPTION}
else
    echo "Unsupported arguent ${OPTION}"
fi

